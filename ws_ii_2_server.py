import asyncio, websockets
import redis.asyncio as redis

from websockets import WebSocketCommonProtocol

redisClient: redis.Redis = redis.Redis(host="127.0.0.1", port=32768, password="redispw")


async def received_websocket(ws: WebSocketCommonProtocol):
    username = await ws.recv()

    await redisClient.set(username, ws.remote_address[0])
    print(f"Added {username} to redis")

    try:
        while not ws.closed:
            msg = await ws.recv()
            print(f"{username}: {msg}")

            await ws.send(f'{username}:{msg}')
    finally:
        await redisClient.set(username, '')
        print(f"Removed {username} from redis")


async def main():
    async with websockets.serve(received_websocket, 'localhost', 8000):
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())
