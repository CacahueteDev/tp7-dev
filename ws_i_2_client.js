let ws = new WebSocket('ws://localhost:8000');

ws.addEventListener('open', function () {
    document.getElementById('wait').classList.add('hidden');
    document.getElementById('chat').classList.remove('hidden');
});

ws.addEventListener('message', function (event) {
    document.getElementById('chat-result').innerText = event.data;
});

function wsSendUsername() {
    let usernameBox = document.getElementById('username-box');

    if (usernameBox.textContent === '') {
        alert('Please enter a username');
        return;
    }

    ws.send(usernameBox.textContent);
    document.getElementById('chat-username').innerText = usernameBox.textContent;

    document.getElementById('connect').classList.add('hidden');
    document.getElementById('chat').classList.remove('hidden');
}

function wsSendChat() {
    let chatBox = document.getElementById('chat-box');
    let chatMessage = chatBox.value;

    if (chatMessage === '') {
        alert('Please enter a message');
        return;
    }

    ws.send(chatMessage);
    chatBox.textContent = '';
}