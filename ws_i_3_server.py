import asyncio, websockets

from websockets import WebSocketCommonProtocol

CLIENTS = []


class Client:
    def __init__(self, ws: WebSocketCommonProtocol, username: str):
        self.ws = ws
        self.username = username

    def get_ws(self):
        return self.ws

    def get_username(self):
        return self.username


async def received_websocket(ws: WebSocketCommonProtocol):
    username = await ws.recv()
    client = Client(ws, username)

    for client in CLIENTS:
        if client.get_username() == username:
            await ws.send(f'Casse toi, {username} est déjà là !')
            await ws.close()

            return

    CLIENTS.append(client)

    try:
        while not ws.closed:
            msg = await ws.recv()
            print(f"{username}: {msg}")

            await ws.send(f'{username}:{msg}')
    finally:
        CLIENTS.remove(client)


async def main():
    async with websockets.serve(received_websocket, 'localhost', 8000):
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())
