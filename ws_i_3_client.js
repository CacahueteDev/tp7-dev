let ws = new WebSocket('ws://localhost:8000');

ws.addEventListener('open', function () {
    document.getElementById('wait').classList.add('hidden');
    document.getElementById('connect').classList.remove('hidden');
});

ws.addEventListener('message', function (event) {
    let data = event.data.split(':');
    let username = data[0];
    let message = data[1];

    document.getElementById('chat-list').innerHTML += `
        <div class="message">
            <b>${username}</b>
            <p>${message}</p>
        </div>
    `;
});

function wsSendUsername() {
    let usernameBox = document.getElementById('username-box');

    if (usernameBox.value === '') {
        alert('Please enter a username');
        return;
    }

    ws.send(usernameBox.value);
    document.getElementById('chat-username').innerText = usernameBox.value;

    document.getElementById('connect').classList.add('hidden');
    document.getElementById('chat').classList.remove('hidden');
}

function wsSendChat() {
    let chatBox = document.getElementById('chat-box');
    let chatMessage = chatBox.value;

    if (chatMessage === '') {
        alert('Please enter a message');
        return;
    }

    ws.send(chatMessage);
    chatBox.textContent = '';
}