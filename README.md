# TP7 Websockets

## 1. First steps

[ws_i_1_server.py](ws_i_1_server.py)

```shell
hello world
```

[ws_i_1_client.py](ws_i_1_client.py)

```shell
What do you want to say: hello world
Server responded with Hello client ! Received "hello world"
```

## 2. Client JS

[ws_i_2_client.js](ws_i_2_client.js)

## 3. Chatroom magueule

J'ai décidé de prendre le client JS (non pitié je suis pas dev web front 😭)

[ws_i_3_server.py](ws_i_3_server.py)

[ws_i_3_client.js](ws_i_2_client.js)

## II. Base de données

[ws_ii_2_server.py](ws_ii_2_server.py)

```shell
$ python ws_ii_2_server.py
Added guy to redis
guy: hello
Removed guy from redis
```