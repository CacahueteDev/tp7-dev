import asyncio, websockets

from websockets import WebSocketCommonProtocol


async def receiveWebsocket(ws: WebSocketCommonProtocol):
    input_value = await ws.recv()
    print(input_value)

    await ws.send(f'Hello client ! Received "{input_value}"')


async def main():
    async with websockets.serve(receiveWebsocket, 'localhost', 8000):
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())