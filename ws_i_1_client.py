import asyncio, websockets


async def main():
    uri = "ws://localhost:8000"
    async with websockets.connect(uri) as ws:
        msg = input("What do you want to say: ")
        await ws.send(msg)

        print(f"Server responded with {await ws.recv()}")


if __name__ == "__main__":
    asyncio.run(main())